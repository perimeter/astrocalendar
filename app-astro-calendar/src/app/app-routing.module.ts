import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainScreenComponent } from './main-screen/main-screen.component';
import { AppComponent } from './app.component';

const routes: Routes =  [ 
   { path: '',
     redirectTo: "main-screen", pathMatch: 'full' //, children: [
    },
    { path: 'main-screen',
        component: MainScreenComponent 
    }
    // ] }
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  declarations: [MainScreenComponent],
  exports: [RouterModule]
})
export class AppRoutingModule {}
